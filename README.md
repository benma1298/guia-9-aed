Guia 9 Algoritmos y Estructura de Datos

Guia Numero 9 Unidad 3
Tema: Metodos de Busqueda - Tablas Hash.
Autor: Benjamin Martin A.
Fecha: 13 de Diciembre, 2021.

En el presente Programa, se requiere almacenar datos numericos en una arreglo de 20 elementos como maximo.
Se escribe un codigo en C++ que permita el ingreso y busqueda de datos en el arreglo, definiendo una funcion Hash, la cual
distribuye los registros de datos en el arreglo, y en caso de haber colisiones, estas se resuelven aplicando los metodos:
    - Reasignacion Prueba Lineal (L).
    - Reasignacion Prueba Cuadratica (C).
    - Reasignacion Doble Direccion Hash (D).
    - Encadenamiento (E).
(El metodo de resolusion de colisiones debe ser determinado al iniciar el programa, digitando la letra asociada al metodo.)

Los pasos para inciar el programa son:
1.- Ejecutar el programa abriendo la terminal.
2.- Escribir en la terminal el comando main seguido de la letra correspondiente al metodo que eligio el usuario. "./main {L|C|D|E}".
    Por ejemplo: ./main L
3.- Interactuar con las opciones del programa a eleccion del usuario.
