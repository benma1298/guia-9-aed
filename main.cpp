
#include <iostream>
#include "busqueda.h"

using namespace std;

// Funcion que Inicia el programa para que el usuario elija la accion a seguir.
string opciones(string opcion, int cant){
    cout << "Metodos de Busqueda" << endl;
    cout << "Agregar Datos ------------- (1)" << endl;
    cout << "Mostrar Datos ------------- (2)" << endl;
    cout << "Buscar Datos -------------- (3)" << endl;
    cout << "Finalizar Busqueda -------- (0)" << endl << endl;
    cout << "Escriba el numero de la Opcion deseada." << endl;
    cin >> opcion;

    return opcion;
}

// Funcion que ingresa el dato.
int agregaDato(int info, string accion){
    cout << accion << endl;
    cout << "Ingrese el dato: ";
    cin >> info;
    
    return info;
}

// Funcion HASH del programa.
int main(int argc, char **argv){
    if (argc < 2){
        cout << "Error, se deben ingresar dos parametros iniciales." << endl;
        return 0;
    }
    string metodoBusqueda = argv[1];
    if (!(metodoBusqueda == "L") && !(metodoBusqueda == "C") && !(metodoBusqueda == "D") && !(metodoBusqueda == "E")){
        cout << "Error, la letra del metodo que desea usar debe estar en mayuscula." << endl;
        return 0;
    }

    string opcion = "\0";
    int cant = 0;
    int info = 0;
    busqueda *colision = new busqueda();

    if (metodoBusqueda == "E"){
        Nodo *nodo = new Nodo();
    }
    int arreglo[20];
    for (int i = 0; i < 20; i++){
        arreglo[i] = -1;
    }
    while (opcion != "0"){
        opcion = opciones(opcion, cant);
        if(opcion == "1"){
            if(cant == 20){
                cout << "La lista de datos esta llena." << endl; 
            }else{
                info = agregaDato(info, "Agregue el dato: ");
                int n = (info % 19);
                if (arreglo[n] == -1){
                    arreglo[n] = info;

                    cout << "Ha sido agregado: " << endl;
                    cout << "El dato: " << arreglo[n] << endl;
                    cout << "En la posicion: " << n << endl << endl;
                }else{
                    cout << "Existe Colision" << endl;
                    cout << "Dato Original: " << arreglo[n] << endl;
                    cout << "Dato Nuevo: " << info << endl;
                    cout << "Posicion de Colision: " << n << endl << endl;

                    if (metodoBusqueda == "L"){
                        n = colision -> PruebaLineal(arreglo, 20, info, n, n);
                        arreglo[n] = info;
                        cout << "La Colision se ha arreglado" << endl;
                        cout << "Dato: " << arreglo[n] << endl;
                        cout << "Posicion: " << n << endl << endl;

                    }else if(metodoBusqueda == "C"){
                        n = colision -> PruebaCuadratica(arreglo, 20, info, n, n, 1);
                        arreglo[n] = info;
                        cout << "La Colision se ha arreglado" << endl;
                        cout << "Dato: " << arreglo[n] << endl;
                        cout << "Posicion: " << n << endl << endl;

                    }else if(metodoBusqueda == "D"){
                        n = colision -> DobleDireccionHash(arreglo, 20, info, n, n);
                        arreglo[n] = info;
                        cout << "La Colision se ha arreglado" << endl;
                        cout << "Dato: " << arreglo[n] << endl;
                        cout << "Posicion: " << n << endl << endl;

                    }else{
                        Nodo *nodo = new Nodo();
                        colision -> Encadenamiento(arreglo, 20, info, n, n);
                    }
                }
                cant++;
            }
        }else if(opcion == "2"){
            cout << "Se mostraran todos los valores del Arreglo." << endl;

            for(int i = 0; i < 20; i++){
                if (arreglo[i] != -1){
                    cout << "[" << arreglo[i] << "]" << " ";
                }else{
                    cout << "[ ]" << " ";
                }
            }
            cout << endl << endl;

        }else if (opcion == "3"){
            info = agregaDato(info, "Buscar Datos.");
            int n = (info % 19);

            if(arreglo[n] == info){
                cout << "El Dato ha sido encontrado: " << arreglo[n] << endl;
                cout << "En la Posicion: " << n << endl << endl;
            }else{
                if(metodoBusqueda == "L"){
                    colision -> busquedaPruebaLineal(arreglo, 20, info, n, n);
                }else if(metodoBusqueda == "C"){
                    colision -> busquedaPruebaCuadratica(arreglo, 20, info, n, n, 1);
                }else if(metodoBusqueda == "D"){
                    colision -> busquedaDobleDireccionHash(arreglo, 20, info, n, n);
                }else{
                    colision -> busquedaEncadenamiento(arreglo, 20, info, n, n);
                }
            }
        }
    }
    return 0;
}