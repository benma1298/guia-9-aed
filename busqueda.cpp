
#include <iostream>
#include "busqueda.h"

using namespace std;

busqueda::busqueda(){

}
// Funciones de Reasignacion Prueba Lineal (L).
int busqueda::PruebaLineal(int arreglo[], int dim, int info, int n, int np){
    n++;
    if (n == dim){
        n = 0;
    }

    if(arreglo[n] == -1){
        return n;
    }else{
        PruebaLineal(arreglo, dim, info, n, np);
    }
} 

void busqueda::busquedaPruebaLineal(int arreglo[], int dim, int info, int n, int np){
    n++;
    if (n == dim){
        n = 0;
    }
    if (n == np){
        cout << "Error, no se encuentra el dato." << endl;
    }
    if (arreglo[n] == info){
        cout << "Ha ocurrido una colision." << endl;
        cout << "El dato corresponde a: " << arreglo[n] << endl;
        cout << "Desplazamiento final: " << n << endl << endl;
    }else{
        busquedaPruebaLineal(arreglo, dim, info, n, np);
    }
}

// Funciones de Reasignacion Prueba Cuadratica (C).
int busqueda::PruebaCuadratica(int arreglo[], int dim, int info, int n, int np, int p){
    n = np + (p*p);
    if (n >= dim){
        p = 0;
        n = 1;
        np = 1;
    }
    if (arreglo[n] == -1){
        return n;
    }else{
        PruebaCuadratica(arreglo, 20, info, n, np, p++);
    }
}

void busqueda::busquedaPruebaCuadratica(int arreglo[], int dim, int info, int n, int np, int p){
    n = np + (p*p);
    while(arreglo[n] != -1 && arreglo[n] != info){
        p++;
        n = np + (p*p);
        if (n >= dim){
            p = 0;
            n = 1;
            np = 1;
        }
    }
    if (arreglo[n] == -1){
        cout << "Error, no se ha encontrado el dato." << endl;
    }else{
        cout << "Ha ocurrido una colision." << endl;
        cout << "El dato corresponde a: " << arreglo[n] << endl;
        cout << "Desplazamiento final: " << n << endl;
    }
}

// Funciones de Reasignacion Doble Direccion Hash (D).
int busqueda::DobleDireccionHash(int arreglo[], int dim, int info, int n, int np){
    n = (np % 19);
    if(arreglo[n] == -1){
        return n;
    }else{
        DobleDireccionHash(arreglo, 20, info, n, n+1);
    }
}

void busqueda::busquedaDobleDireccionHash(int arreglo[], int dim, int info, int n, int np){
    n = (np % 19);

    while((arreglo[n] != info) && (n != np) && (n < dim) && (arreglo[n] != -1)){
        n = ((n + 1) % 19);
    }
    if((arreglo[n] != info) || (arreglo[n] == -1)){
        cout << "Error, no se ha encontrado el dato." << endl;
    }else{
        cout << "Ha ocurrido una colision." << endl;
        cout << "El dato corresponde a: " << arreglo[n] << endl;
        cout << "Desplazamiento final: " << n << endl;
    }
}

// Funciones Encadenamiento (E).
void busqueda::Encadenamiento(int arreglo[], int dim, int info, int n, int np){
    Nodo *nodo = new Nodo();

    if (nodo == NULL){
        nodo -> info = arreglo[np];
    }
    while((nodo -> info != info) && (nodo != NULL)){
        nodo = nodo -> next;
    }

    if(nodo == NULL){
        nodo -> info = arreglo[np];
    }
}

void busqueda::busquedaEncadenamiento(int arreglo[], int dim, int info, int n, int np){
    Nodo *nodo = new Nodo();

    while((nodo -> info != info) && (nodo != NULL)){
        nodo = nodo -> next;
    }
    if(nodo != NULL){
        cout << "Error, No se ha encontrado el dato." << endl;

    }else{
        cout << "Se ha encontrado el dato en la posicion: " << n << endl;
    } 
}