#ifndef BUSQUEDA_H
#define BUSQUEDA_H
#include <iostream>

using namespace std;

typedef struct Nodo{
    int info;
    struct Nodo *next;
} Nodo;

class busqueda{
    private:
    Nodo *first = NULL;

    public:
    busqueda();

    // Funciones Reasignacion Prueba Lineal (L).
    int PruebaLineal(int arreglo[], int dim, int info, int n, int np);
    void busquedaPruebaLineal(int arreglo[], int dim, int info, int n, int np);

    // Funciones Reasignacion Prueba Cuadratica (C).
    int PruebaCuadratica(int arreglo[], int dim, int info, int n, int np, int p);
    void busquedaPruebaCuadratica(int arreglo[], int dim, int info, int n, int np, int p);

    // Funciones Reasignacion Doble Direccion Hash (D).
    int DobleDireccionHash(int arreglo[], int dim, int info, int n, int np);
    void busquedaDobleDireccionHash(int arreglo[], int dim, int info, int n, int np);

    // Funciones Encadenamiento (E).
    void Encadenamiento(int arreglo[], int dim, int info, int n, int np);
    void busquedaEncadenamiento(int arreglo[], int dim, int info, int n, int np);
};
#endif